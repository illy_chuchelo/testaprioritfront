import { Component, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';
import { Position } from '../models/position';
import { PositionDataService } from '../srevices/position/position.data.services';


@Component({
  selector: 'app-position',
  templateUrl: './position.component.html',
  styleUrls: ['./position.component.css']
})
export class PositionComponent implements OnInit {

  position: Position = new Position;

  name = new FormControl('', Validators.required);
  constructor(public dialogRef: MatDialogRef<PositionComponent>,
    private PositionDataService: PositionDataService) { }

  ngOnInit(): void {

  }

  createPosition() {
    if (!this.name.hasError('required')) {
      this.PositionDataService.createPosition(this.position)
        .subscribe();
      this.dialogRef.close();
    }
  }

}
