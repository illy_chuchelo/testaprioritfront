import { Position } from "./position";

export class Person {
  constructor(
    public id?: number,
    public name?: string,
    public surname?: string,
    public position?: Position,
    public hiringDate?: any,
    public fireDate?: any,
    public salary?: number,
  ) { }
}
