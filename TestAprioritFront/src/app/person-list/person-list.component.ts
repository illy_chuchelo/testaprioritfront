import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatTableDataSource } from '@angular/material/table';
import { Person } from '../models/person';
import { Position } from '../models/position';
import { PositionComponent } from '../position/position.component';
import { PersonComponent } from '../person/person.component';
import { PersonDataService } from '../srevices/person/person.data.services';


@Component({
  selector: 'app-person-list',
  templateUrl: './person-list.component.html',
  styleUrls: ['./person-list.component.css'],
  providers: [PersonDataService]
})

export class PersonListComponent implements OnInit {

  persons?: Person[];

  displayedColumns: string[] = ['id', 'position', 'name', 'salary', 'heringDate', 'fireDate'];
  dataSource = new MatTableDataSource();

  constructor(private PersonDataService: PersonDataService,
    public MatDialog: MatDialog
  ) { }

  ngOnInit(): void {

    this.loadPerson();
  }

  loadPerson() {

    this.PersonDataService.getPositions()
      .subscribe((data: Person[]) => this.dataSource.data = data);

  }

  openAddPersonDialog() {

    const dialogRef = this.MatDialog.open(PersonComponent);
    dialogRef.afterClosed().subscribe(result => {
      this.loadPerson();
    });
  }

  openAddPositionDialog() {

    this.MatDialog.open(PositionComponent)

  }

}
