import { Component, OnInit } from '@angular/core';
import { PositionDataService } from '../srevices/position/position.data.services';
import { PersonDataService } from '../srevices/person/person.data.services';
import { FormControl, Validators } from '@angular/forms';
import { Position } from '../models/position';
import { Person } from '../models/person';
import { MatDialogRef } from '@angular/material/dialog';


@Component({
  selector: 'app-person',
  templateUrl: './person.component.html',
  styleUrls: ['./person.component.css']
})
export class PersonComponent implements OnInit {  

  person: Person = new Person;
  positions?: Position[];

  hiringDate = new FormControl(new Date().toISOString(), Validators.required);
  fireDate = new FormControl(new Date().toISOString());
  surname = new FormControl('', Validators.required);
  name = new FormControl('', Validators.required);
  position = new FormControl('', Validators.required);
  salary = new FormControl('', Validators.required);

  constructor(public dialogRef: MatDialogRef<PersonComponent>,
    private PositionDataService: PositionDataService,
    private PersonDataService: PersonDataService
  ) { }

  ngOnInit(): void {

    this.loadPosition();

  }

  loadPosition() {

    this.PositionDataService.getPositions()
      .subscribe((data: Position[]) => this.positions = data);

  }

  createPerson() {

    if (!this.name.hasError('required')
      && !this.name.hasError('required')
      && !this.surname.hasError('required')
      && !this.position.hasError('required')
      && !this.salary.hasError('required')
      && !this.hiringDate.hasError('required')) {

      this.PersonDataService.createPerson(this.person)
        .subscribe();
      this.dialogRef.close();
    }

  }

}
