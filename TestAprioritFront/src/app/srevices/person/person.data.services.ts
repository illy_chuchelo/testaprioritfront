import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Constants } from '../../constants/constants';
import { Person } from '../../models/person';
import { DatePipe } from '@angular/common';

@Injectable()
export class PersonDataService {
  private url = "/api/persons";

  constructor(private http: HttpClient,
    private datePipe: DatePipe) { }


  getPositions() {

    return this.http.get<any>(Constants.BASE_URL + this.url);

  }

  createPerson(person: Person) {

    const body = {
      positionId: person.position?.id,
      name: person.name,
      surname: person.surname,
      hiringDate: person.hiringDate,
      fireDate: person.fireDate,
      salary: person.salary,
    }

    return this.http.post<any>(Constants.BASE_URL + this.url, body);

  }

}
