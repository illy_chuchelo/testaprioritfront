import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Constants } from '../../constants/constants';
import { Position } from '../../models/position';

@Injectable()
export class PositionDataService {
  private url = "/api/positions";

  constructor(private http: HttpClient) { }


  getPositions() {
    
    return this.http.get<any>(Constants.BASE_URL + this.url);

  }

  createPosition(position: Position) {

    const body = {      
      name: position.name,     
    }

    return this.http.post<any>(Constants.BASE_URL + this.url, body);
  }

}
